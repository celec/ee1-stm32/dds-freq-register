#define usart1
#include "uart.h"

void send_char(char c){
	signed char v, k;
	for (k = 1; k >= 0; k--){ 
		v = (c >> (4*k)) & 0xf;
		if (v < 10){ my_putchar(v + '0');}
		else { my_putchar(v - 10 + 'A');}
	}
}

void send_short(short s){
	send_char(s >> 8); // MSBs
	send_char(s & 0xff); // LSBs
}
