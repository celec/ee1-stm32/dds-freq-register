void usart_setup(void);
void init_gpio(void);
void clock_setup(void);
void my_putchar(unsigned char);
void led_set();
void led_clr();
void delay(unsigned int);
void big_delay(char);
void my_puts(char *);
