#include "communications.h" // send_char(), send_short()
#include "uart.h" // contains uart_*.c prototypes

/* Compute the two 14bits words needed to set the 70 MHz DDS AD9834 at 4MHz, store in a 32b word
 *
 * Control Register (DBx): FSEL @ DB11
 *
 * DDS Output = FREG(28b) * Fdds / 2^28
 * hence FREG = 4MHz * 2^28 / 70MHz = 4 * 2^28 / 70
 * ==> FREG = ((0x1 << 28) << 2) >> 7 + ((128-70) >> 7)
 *
 * For the general case (not only 4MHz):
 * octave: rats(2^28 / 70e6) = 3505/914, reduce to avoid overflow
 * ==> FREG = freq * 3505 / 914
 * 
 * but 4e6 * 3505 is above 32b => rats(2^28 / 70e6, 8) = 441 / 115,
 * worst resolution in the LSBs, but in the range of a long
 */   

int main(void) {
	int msk = (1 << 11) | (1 << 12) | (1 << 13);
	char newline[3] = "\r\n\0";
	unsigned long F; // whole 32b word
	short F1, F2; // the two 14b words where F is splitted

	const long long f_wanted = 4e6; // to support 4e6 * 3505, see above

	clock_setup();
	init_gpio();
	usart_setup();

	F = (f_wanted * 3505) / 914; // see above
	F1 = ((F >> 14) & 0x3fff) | 0x8000;
	F2 = ((F >> 0 ) & 0x3fff) | 0x4000;

	while(1){
		led_set(msk);
		big_delay(8);

		send_short(F1);
		my_putchar(' ');
		send_short(F2);
		my_puts(newline);

		led_clr(msk);
		big_delay(8);
	}
	
	return 0;
}
